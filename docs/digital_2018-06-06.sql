# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: production-v2.cxdmapgx0cph.us-east-1.rds.amazonaws.com (MySQL 5.6.27-log)
# Database: digital
# Generation Time: 2018-06-06 13:21:25 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table creatives
# ------------------------------------------------------------

DROP TABLE IF EXISTS `creatives`;

CREATE TABLE `creatives` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` char(36) NOT NULL DEFAULT '',
  `image_url` varchar(256) NOT NULL DEFAULT '',
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ndx_uuid` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `creatives` WRITE;
/*!40000 ALTER TABLE `creatives` DISABLE KEYS */;

INSERT INTO `creatives` (`id`, `uuid`, `image_url`, `width`, `height`)
VALUES
	(1,'2386d3f6-1ca5-406a-ba9f-fe07316ec330','http://support.digital.flytedesk.com/creatives/banner_test_1.png',728,90),
	(2,'8b6b5955-d8c5-485a-9637-120eb91225d0','http://support.digital.flytedesk.com/creatives/banner_test_2.png',728,90),
	(3,'bb184d46-3562-4765-b2c6-5a4b0f93de06','http://support.digital.flytedesk.com/creatives/banner_test_3.png',728,90),
	(4,'1fcf315d-5474-472d-9462-5dae8472ea78','http://support.digital.flytedesk.com/creatives/medium-rect-1.png',350,250),
	(5,'50f94785-d43f-4b36-9842-13e2b0087d2d','http://support.digital.flytedesk.com/creatives/medium-rect-2.png',350,250),
	(6,'15184bf8-ce98-4822-8aee-7d19179ff48d','http://support.digital.flytedesk.com/creatives/medium-rect-3.png',350,250);

/*!40000 ALTER TABLE `creatives` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table properties
# ------------------------------------------------------------

DROP TABLE IF EXISTS `properties`;

CREATE TABLE `properties` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` char(36) NOT NULL DEFAULT '',
  `base_url` varchar(256) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `ndx_uuid` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `properties` WRITE;
/*!40000 ALTER TABLE `properties` DISABLE KEYS */;

INSERT INTO `properties` (`id`, `uuid`, `base_url`)
VALUES
	(1,'9594be66-f69c-4d05-9868-d64b0829487a','http://test.digital.flytedesk.com/test1'),
	(2,'277d91cc-103f-442d-8ff5-b48543a68516','http://test.digital.flytedesk.com/test2');

/*!40000 ALTER TABLE `properties` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table property_unit_creatives
# ------------------------------------------------------------

DROP TABLE IF EXISTS `property_unit_creatives`;

CREATE TABLE `property_unit_creatives` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `uuid` char(36) NOT NULL DEFAULT '',
  `max_impressions` int(11) DEFAULT NULL,
  `total_impressions` int(11) DEFAULT NULL,
  `property_unit_uuid` char(36) NOT NULL DEFAULT '',
  `link` varchar(256) NOT NULL DEFAULT '',
  `creative_uuid` char(36) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `ndx_uuid` (`uuid`),
  KEY `ndx_property_unit` (`property_unit_uuid`),
  KEY `ndx_creative` (`creative_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table property_units
# ------------------------------------------------------------

DROP TABLE IF EXISTS `property_units`;

CREATE TABLE `property_units` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `property_uuid` char(36) NOT NULL DEFAULT '',
  `unit_type_id` int(11) unsigned NOT NULL,
  `section` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `property_units` WRITE;
/*!40000 ALTER TABLE `property_units` DISABLE KEYS */;

INSERT INTO `property_units` (`id`, `property_uuid`, `unit_type_id`, `section`)
VALUES
	(1,'9594be66-f69c-4d05-9868-d64b0829487a',1,'top'),
	(2,'277d91cc-103f-442d-8ff5-b48543a68516',1,'top'),
	(3,'9594be66-f69c-4d05-9868-d64b0829487a',2,'midpage'),
	(4,'277d91cc-103f-442d-8ff5-b48543a68516',2,'midpage'),
	(5,'9594be66-f69c-4d05-9868-d64b0829487a',1,'below fold'),
	(6,'277d91cc-103f-442d-8ff5-b48543a68516',1,'below fold');

/*!40000 ALTER TABLE `property_units` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sessions`;

CREATE TABLE `sessions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` char(36) NOT NULL,
  `ip_address` char(15) NOT NULL DEFAULT '',
  `visitor_uuid` char(36) DEFAULT NULL,
  `user_agent` text,
  `referrer` text,
  `url` text,
  `date_created` timestamp NOT NULL,
  `date_updated` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ndx_ip_address` (`ip_address`),
  KEY `ndx_visitor_uid` (`visitor_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;

INSERT INTO `sessions` (`id`, `uuid`, `ip_address`, `visitor_uuid`, `user_agent`, `referrer`, `url`, `date_created`, `date_updated`)
VALUES
	(1,'81baef07-38d1-44f6-b55a-7be8ee516d5c','172.31.40.45','5632b279-282e-4c75-8c57-1950a3622bd8','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36',NULL,NULL,'2018-06-06 13:06:52','2018-06-06 13:06:52'),
	(2,'a9a6fceb-7109-48e6-ad4f-ff6fdc70de73','172.31.4.159','0f45417c-b0c3-48d5-8a52-c79f60ce61c4','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36',NULL,NULL,'2018-06-06 13:09:46','2018-06-06 13:09:46'),
	(3,'247ca669-b9fb-48ef-b2be-73f6a80e02d7','172.31.4.159','b95dba60-b6ec-449e-8f05-1360f4956d88','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36',NULL,NULL,'2018-06-06 13:10:40','2018-06-06 13:10:40'),
	(4,'13fea178-c0ab-47cc-904e-e440b1af9ee7','172.31.4.159','4429a75c-7ab6-4f3e-b1b2-b7f453be6564','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36',NULL,NULL,'2018-06-06 13:10:42','2018-06-06 13:10:42'),
	(5,'0b934da6-b1be-44dc-b5fe-4c367e5d4aaa','172.31.4.159','86707ed2-e3d3-42c1-954d-9bfed7a9fa9d','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36',NULL,NULL,'2018-06-06 13:10:45','2018-06-06 13:10:45');

/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table units
# ------------------------------------------------------------

DROP TABLE IF EXISTS `units`;

CREATE TABLE `units` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `class_name` varchar(256) NOT NULL DEFAULT '',
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `units` WRITE;
/*!40000 ALTER TABLE `units` DISABLE KEYS */;

INSERT INTO `units` (`id`, `name`, `class_name`, `width`, `height`)
VALUES
	(1,'Leaderboard','fly_lb',728,90),
	(2,'Medium Rectangle','fly_mr',300,250),
	(9,'Large Rectange','fly_lr',336,280),
	(10,'Half Page','fly_hp',300,600),
	(12,'Skyscraper','fly_sk',120,600),
	(13,'Wide Skyscraper','fly_ws',160,600),
	(14,'Rectangle','fly_rc',180,150);

/*!40000 ALTER TABLE `units` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table visitors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `visitors`;

CREATE TABLE `visitors` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` char(36) NOT NULL DEFAULT '',
  `date_created` timestamp NOT NULL,
  `date_updated` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `visitors` WRITE;
/*!40000 ALTER TABLE `visitors` DISABLE KEYS */;

INSERT INTO `visitors` (`id`, `uuid`, `date_created`, `date_updated`)
VALUES
	(1,'5632b279-282e-4c75-8c57-1950a3622bd8','2018-06-06 13:06:52','2018-06-06 13:06:52'),
	(2,'0f45417c-b0c3-48d5-8a52-c79f60ce61c4','2018-06-06 13:09:46','2018-06-06 13:09:46'),
	(3,'b95dba60-b6ec-449e-8f05-1360f4956d88','2018-06-06 13:10:40','2018-06-06 13:10:40'),
	(4,'4429a75c-7ab6-4f3e-b1b2-b7f453be6564','2018-06-06 13:10:42','2018-06-06 13:10:42'),
	(5,'86707ed2-e3d3-42c1-954d-9bfed7a9fa9d','2018-06-06 13:10:45','2018-06-06 13:10:45');

/*!40000 ALTER TABLE `visitors` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
