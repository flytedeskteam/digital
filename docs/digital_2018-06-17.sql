# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: production-v2.cxdmapgx0cph.us-east-1.rds.amazonaws.com (MySQL 5.6.27-log)
# Database: digital
# Generation Time: 2018-06-17 19:41:27 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table ad_units
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ad_units`;

CREATE TABLE `ad_units` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `class_name` varchar(256) NOT NULL DEFAULT '',
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `date_created` timestamp NOT NULL,
  `date_updated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ndx_class` (`class_name`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `ad_units` WRITE;
/*!40000 ALTER TABLE `ad_units` DISABLE KEYS */;

INSERT INTO `ad_units` (`id`, `name`, `class_name`, `width`, `height`, `date_created`, `date_updated`, `deleted_at`)
VALUES
	(1,'Leaderboard','fly_lb',728,90,'0000-00-00 00:00:00',NULL,NULL),
	(2,'Medium Rectangle','fly_mr',300,250,'0000-00-00 00:00:00',NULL,NULL),
	(9,'Large Rectange','fly_lr',336,280,'0000-00-00 00:00:00',NULL,NULL),
	(10,'Half Page','fly_hp',300,600,'0000-00-00 00:00:00',NULL,NULL),
	(12,'Skyscraper','fly_sk',120,600,'0000-00-00 00:00:00',NULL,NULL),
	(13,'Wide Skyscraper','fly_ws',160,600,'0000-00-00 00:00:00',NULL,NULL),
	(14,'Rectangle','fly_rc',180,150,'0000-00-00 00:00:00',NULL,NULL);

/*!40000 ALTER TABLE `ad_units` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table buyers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `buyers`;

CREATE TABLE `buyers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` char(36) NOT NULL DEFAULT '',
  `name` varchar(256) DEFAULT '',
  `date_created` timestamp NULL DEFAULT NULL,
  `date_updated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ndx_uuid` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table clicks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `clicks`;

CREATE TABLE `clicks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `session_uuid` char(36) NOT NULL DEFAULT '',
  `impression_uuid` char(36) NOT NULL DEFAULT '',
  `click_url` varchar(256) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `date_updated` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table creatives
# ------------------------------------------------------------

DROP TABLE IF EXISTS `creatives`;

CREATE TABLE `creatives` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` char(36) NOT NULL DEFAULT '',
  `image_url` varchar(256) NOT NULL DEFAULT '',
  `unit_uuid` char(36) DEFAULT NULL,
  `click_url` varchar(256) DEFAULT NULL,
  `date_created` timestamp NOT NULL,
  `date_updated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ndx_uuid` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `creatives` WRITE;
/*!40000 ALTER TABLE `creatives` DISABLE KEYS */;

INSERT INTO `creatives` (`id`, `uuid`, `image_url`, `unit_uuid`, `click_url`, `date_created`, `date_updated`, `deleted_at`)
VALUES
	(1,'2386d3f6-1ca5-406a-ba9f-fe07316ec330','http://support.digital.flytedesk.com/creatives/banner_test_1.png','1',NULL,'0000-00-00 00:00:00',NULL,NULL),
	(2,'8b6b5955-d8c5-485a-9637-120eb91225d0','http://support.digital.flytedesk.com/creatives/banner_test_2.png','1',NULL,'0000-00-00 00:00:00',NULL,NULL),
	(3,'bb184d46-3562-4765-b2c6-5a4b0f93de06','http://support.digital.flytedesk.com/creatives/banner_test_3.png','1',NULL,'0000-00-00 00:00:00',NULL,NULL),
	(4,'1fcf315d-5474-472d-9462-5dae8472ea78','http://support.digital.flytedesk.com/creatives/medium-rect-1.png','2',NULL,'0000-00-00 00:00:00',NULL,NULL),
	(5,'50f94785-d43f-4b36-9842-13e2b0087d2d','http://support.digital.flytedesk.com/creatives/medium-rect-2.png','2',NULL,'0000-00-00 00:00:00',NULL,NULL),
	(6,'15184bf8-ce98-4822-8aee-7d19179ff48d','http://support.digital.flytedesk.com/creatives/medium-rect-3.png','2',NULL,'0000-00-00 00:00:00',NULL,NULL),
	(7,'blah','http://support.digital.flytedesk.com/creatives/skyscraper.png','12',NULL,'0000-00-00 00:00:00',NULL,NULL);

/*!40000 ALTER TABLE `creatives` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table impressions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `impressions`;

CREATE TABLE `impressions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `session_uuid` char(36) NOT NULL DEFAULT '',
  `remnant_uuid` char(36) DEFAULT NULL,
  `property_creatives_uuid` char(36) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `date_updated` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `impressions` WRITE;
/*!40000 ALTER TABLE `impressions` DISABLE KEYS */;

INSERT INTO `impressions` (`id`, `session_uuid`, `remnant_uuid`, `property_creatives_uuid`, `date_created`, `date_updated`)
VALUES
	(1,'705423b1-e8f8-44c7-b157-3d4eeffc0cea','','9594be66-f69c-4d05-9868-d64b0829487a','2018-06-06 17:17:55','2018-06-06 17:17:55'),
	(2,'705423b1-e8f8-44c7-b157-3d4eeffc0cea','','9594be66-f69c-4d05-9868-d64b0829487a','2018-06-06 17:17:56','2018-06-06 17:17:56'),
	(3,'e4a558ba-3204-4b4c-96a5-239a814a83fc','','9594be66-f69c-4d05-9868-d64b0829487a','2018-06-06 17:20:35','2018-06-06 17:20:35'),
	(4,'e4a558ba-3204-4b4c-96a5-239a814a83fc','','9594be66-f69c-4d05-9868-d64b0829487a','2018-06-06 17:20:35','2018-06-06 17:20:35'),
	(5,'72250a77-4dee-43c6-8d73-98c968b84bec','','9594be66-f69c-4d05-9868-d64b0829487a','2018-06-06 18:57:05','2018-06-06 18:57:05'),
	(6,'72250a77-4dee-43c6-8d73-98c968b84bec','','9594be66-f69c-4d05-9868-d64b0829487a','2018-06-06 18:57:05','2018-06-06 18:57:05');

/*!40000 ALTER TABLE `impressions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table properties
# ------------------------------------------------------------

DROP TABLE IF EXISTS `properties`;

CREATE TABLE `properties` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` char(36) NOT NULL DEFAULT '',
  `name` varchar(256) DEFAULT '',
  `base_url` varchar(256) NOT NULL DEFAULT '',
  `date_created` timestamp NULL DEFAULT NULL,
  `date_updated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ndx_uuid` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `properties` WRITE;
/*!40000 ALTER TABLE `properties` DISABLE KEYS */;

INSERT INTO `properties` (`id`, `uuid`, `name`, `base_url`, `date_created`, `date_updated`, `deleted_at`)
VALUES
	(1,'9594be66-f69c-4d05-9868-d64b0829487a','','http://troycobb.name',NULL,NULL,NULL);

/*!40000 ALTER TABLE `properties` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table property_creatives
# ------------------------------------------------------------

DROP TABLE IF EXISTS `property_creatives`;

CREATE TABLE `property_creatives` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `uuid` char(36) NOT NULL DEFAULT '',
  `max_impressions` int(11) DEFAULT NULL,
  `total_impressions` int(11) DEFAULT NULL,
  `creative_uuid` char(36) NOT NULL DEFAULT '',
  `property_uuid` char(36) NOT NULL DEFAULT '',
  `unit_uuid` char(36) NOT NULL DEFAULT '',
  `buyer_uuid` char(36) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `date_updated` timestamp NULL DEFAULT NULL,
  `date_maxed` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ndx_uuid` (`uuid`),
  KEY `ndx_creative` (`creative_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `property_creatives` WRITE;
/*!40000 ALTER TABLE `property_creatives` DISABLE KEYS */;

INSERT INTO `property_creatives` (`id`, `name`, `uuid`, `max_impressions`, `total_impressions`, `creative_uuid`, `property_uuid`, `unit_uuid`, `buyer_uuid`, `date_created`, `date_updated`, `date_maxed`, `deleted_at`)
VALUES
	(1,NULL,'1',10,1,'8b6b5955-d8c5-485a-9637-120eb91225d0','9594be66-f69c-4d05-9868-d64b0829487a','1',NULL,NULL,'2018-06-06 17:17:55',NULL,NULL),
	(2,NULL,'2',10,1,'2386d3f6-1ca5-406a-ba9f-fe07316ec330','9594be66-f69c-4d05-9868-d64b0829487a','1',NULL,NULL,'2018-06-06 18:57:05',NULL,NULL),
	(3,NULL,'3',10,1,'bb184d46-3562-4765-b2c6-5a4b0f93de06','9594be66-f69c-4d05-9868-d64b0829487a','1',NULL,NULL,'2018-06-06 17:20:35',NULL,NULL),
	(4,NULL,'4',2,1,'1fcf315d-5474-472d-9462-5dae8472ea78','9594be66-f69c-4d05-9868-d64b0829487a','2',NULL,NULL,'2018-06-06 18:57:05',NULL,NULL),
	(5,NULL,'5',7,1,'50f94785-d43f-4b36-9842-13e2b0087d2d','9594be66-f69c-4d05-9868-d64b0829487a','2',NULL,NULL,'2018-06-06 17:20:35',NULL,NULL),
	(6,NULL,'6',8,1,'15184bf8-ce98-4822-8aee-7d19179ff48d','9594be66-f69c-4d05-9868-d64b0829487a','2',NULL,NULL,'2018-06-06 17:17:55',NULL,NULL);

/*!40000 ALTER TABLE `property_creatives` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table property_units
# ------------------------------------------------------------

DROP TABLE IF EXISTS `property_units`;

CREATE TABLE `property_units` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `property_uuid` char(36) NOT NULL DEFAULT '',
  `unit_uuid` int(36) unsigned NOT NULL,
  `section` varchar(256) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL,
  `date_updated` timestamp NULL DEFAULT NULL,
  `last_verified` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `property_units` WRITE;
/*!40000 ALTER TABLE `property_units` DISABLE KEYS */;

INSERT INTO `property_units` (`id`, `property_uuid`, `unit_uuid`, `section`, `is_active`, `date_created`, `date_updated`, `last_verified`, `deleted_at`)
VALUES
	(1,'9594be66-f69c-4d05-9868-d64b0829487a',1,'top',0,'2018-06-06 23:46:49',NULL,'2018-06-06 23:26:40',NULL),
	(2,'277d91cc-103f-442d-8ff5-b48543a68516',1,'top',0,'2018-06-06 23:46:49',NULL,'2018-06-06 23:26:40',NULL),
	(3,'9594be66-f69c-4d05-9868-d64b0829487a',2,'midpage',0,'2018-06-06 23:46:49',NULL,'2018-06-06 23:26:40',NULL),
	(4,'277d91cc-103f-442d-8ff5-b48543a68516',2,'midpage',0,'2018-06-06 23:46:49',NULL,'2018-06-06 23:26:40',NULL),
	(5,'9594be66-f69c-4d05-9868-d64b0829487a',1,'below fold',0,'2018-06-06 23:46:49',NULL,'2018-06-06 23:26:40',NULL),
	(6,'277d91cc-103f-442d-8ff5-b48543a68516',1,'below fold',0,'2018-06-06 23:46:49',NULL,'2018-06-06 23:26:40',NULL);

/*!40000 ALTER TABLE `property_units` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table remnants
# ------------------------------------------------------------

DROP TABLE IF EXISTS `remnants`;

CREATE TABLE `remnants` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` char(36) NOT NULL DEFAULT '',
  `image_url` varchar(256) NOT NULL DEFAULT '',
  `unit_uuid` char(36) NOT NULL DEFAULT '',
  `click_url` varchar(256) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `ndx_uuid` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `remnants` WRITE;
/*!40000 ALTER TABLE `remnants` DISABLE KEYS */;

INSERT INTO `remnants` (`id`, `uuid`, `image_url`, `unit_uuid`, `click_url`)
VALUES
	(1,'','','','');

/*!40000 ALTER TABLE `remnants` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table visitor_sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `visitor_sessions`;

CREATE TABLE `visitor_sessions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` char(36) NOT NULL,
  `ip_address` char(15) NOT NULL DEFAULT '',
  `visitor_uuid` char(36) DEFAULT NULL,
  `property_uuid` varchar(36) NOT NULL,
  `user_agent` text,
  `referrer` text,
  `url` text,
  `date_created` timestamp NOT NULL,
  `date_updated` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ndx_ip_address` (`ip_address`),
  KEY `ndx_visitor_uid` (`visitor_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `visitor_sessions` WRITE;
/*!40000 ALTER TABLE `visitor_sessions` DISABLE KEYS */;

INSERT INTO `visitor_sessions` (`id`, `uuid`, `ip_address`, `visitor_uuid`, `property_uuid`, `user_agent`, `referrer`, `url`, `date_created`, `date_updated`)
VALUES
	(1,'8cc32f1b-594d-42f0-9d29-ddba89fd80ca','172.31.4.159','5502581f-9299-4bc6-b399-34e9b7d1e9e3','','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36',NULL,NULL,'2018-06-06 16:30:12','2018-06-06 16:30:12'),
	(2,'92521a8d-a33b-4100-a820-e801e50e5345','172.31.4.159','5502581f-9299-4bc6-b399-34e9b7d1e9e3','','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36',NULL,NULL,'2018-06-06 16:32:33','2018-06-06 16:32:33'),
	(3,'2aa2659a-8a58-43e9-9f88-5ae2abf3b929','172.31.40.45','5502581f-9299-4bc6-b399-34e9b7d1e9e3','','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36',NULL,NULL,'2018-06-06 16:56:35','2018-06-06 16:56:35'),
	(4,'2a4815c4-d316-4562-833c-ec6a3d8a1ee2','172.31.40.45','5502581f-9299-4bc6-b399-34e9b7d1e9e3','','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36',NULL,NULL,'2018-06-06 16:56:44','2018-06-06 16:56:44'),
	(5,'705423b1-e8f8-44c7-b157-3d4eeffc0cea','172.31.4.159','5502581f-9299-4bc6-b399-34e9b7d1e9e3','','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36',NULL,NULL,'2018-06-06 17:17:55','2018-06-06 17:17:55'),
	(6,'e4a558ba-3204-4b4c-96a5-239a814a83fc','172.31.4.159','5502581f-9299-4bc6-b399-34e9b7d1e9e3','','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36',NULL,NULL,'2018-06-06 17:20:35','2018-06-06 17:20:35'),
	(7,'72250a77-4dee-43c6-8d73-98c968b84bec','172.31.40.45','5502581f-9299-4bc6-b399-34e9b7d1e9e3','','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36',NULL,NULL,'2018-06-06 18:57:05','2018-06-06 18:57:05'),
	(8,'a75f3658-464c-4fff-b763-74ea01320e18','172.31.4.159','5502581f-9299-4bc6-b399-34e9b7d1e9e3','','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36',NULL,NULL,'2018-06-17 17:30:58','2018-06-17 17:30:58'),
	(9,'6bb6914c-383f-45f8-9477-20233065639f','172.31.4.159','5502581f-9299-4bc6-b399-34e9b7d1e9e3','','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36',NULL,NULL,'2018-06-17 17:39:25','2018-06-17 17:39:25'),
	(10,'8e542302-df6f-48df-8241-e2f0fe02fd46','172.31.40.45','5502581f-9299-4bc6-b399-34e9b7d1e9e3','','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36',NULL,NULL,'2018-06-17 17:54:17','2018-06-17 17:54:17'),
	(11,'6d51fb7a-da03-45f8-a4c4-c108d52bd4e1','172.31.40.45','5502581f-9299-4bc6-b399-34e9b7d1e9e3','','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36',NULL,NULL,'2018-06-17 18:10:51','2018-06-17 18:10:51'),
	(12,'f9ae49e3-6a1f-4b06-aff7-f5ac5eff4e02','172.31.4.159','5502581f-9299-4bc6-b399-34e9b7d1e9e3','','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36',NULL,NULL,'2018-06-17 18:24:15','2018-06-17 18:24:15'),
	(13,'22e79adc-e532-46af-b558-b45c140fb33d','172.31.40.45','5502581f-9299-4bc6-b399-34e9b7d1e9e3','','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36',NULL,NULL,'2018-06-17 18:35:19','2018-06-17 18:35:19'),
	(14,'b111a8c4-2786-4763-b533-b63dbf5fb5ef','172.31.40.45','5502581f-9299-4bc6-b399-34e9b7d1e9e3','','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36',NULL,NULL,'2018-06-17 18:38:54','2018-06-17 18:38:54'),
	(15,'6406b1d5-b6bb-4c05-8bdf-28c457eb977c','172.31.40.45','5502581f-9299-4bc6-b399-34e9b7d1e9e3','','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36',NULL,NULL,'2018-06-17 18:52:23','2018-06-17 18:52:23'),
	(16,'73f356df-2cfe-449e-be96-5f9c3645f1e7','172.31.40.45','5502581f-9299-4bc6-b399-34e9b7d1e9e3','','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36',NULL,NULL,'2018-06-17 19:05:19','2018-06-17 19:05:19'),
	(17,'ca63008c-113c-4414-a91c-afbc8a40fe80','172.31.40.45','5502581f-9299-4bc6-b399-34e9b7d1e9e3','','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36',NULL,NULL,'2018-06-17 19:09:55','2018-06-17 19:09:55'),
	(18,'12f872e9-fbd0-4ea7-87dd-21ccd23d4b03','172.31.40.45','5502581f-9299-4bc6-b399-34e9b7d1e9e3','','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36',NULL,NULL,'2018-06-17 19:33:33','2018-06-17 19:33:33');

/*!40000 ALTER TABLE `visitor_sessions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table visitors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `visitors`;

CREATE TABLE `visitors` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` char(36) NOT NULL DEFAULT '',
  `date_created` timestamp NOT NULL,
  `date_updated` timestamp NULL DEFAULT NULL,
  `last_impression` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `visitors` WRITE;
/*!40000 ALTER TABLE `visitors` DISABLE KEYS */;

INSERT INTO `visitors` (`id`, `uuid`, `date_created`, `date_updated`, `last_impression`)
VALUES
	(1,'5502581f-9299-4bc6-b399-34e9b7d1e9e3','2018-06-06 16:30:12','2018-06-06 16:30:12','2018-06-06 23:23:06');

/*!40000 ALTER TABLE `visitors` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
