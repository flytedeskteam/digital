<?php

namespace App\Services\Cors;

use Spatie\Cors\CorsProfile\DefaultProfile;

class CustomCorsProfile extends DefaultProfile {
	public function addCorsHeaders($response)
	{
		$response->headers->set('Access-Control-Allow-Origin', $this->allowedOriginsToString());
		$response->headers->set('Access-Control-Expose-Headers', $this->toString($this->exposeHeaders()));
		$response->headers->set('Access-Control-Allow-Credentials', "true");

		return $response;
	}

	public function addPreflightHeaders($response)
	{
		$response->headers->set('Access-Control-Allow-Methods', $this->toString($this->allowMethods()));
		$response->headers->set('Access-Control-Allow-Headers', $this->toString($this->allowHeaders()));
		$response->headers->set('Access-Control-Allow-Origin', $this->allowedOriginsToString());
		$response->headers->set('Access-Control-Allow-Credentials', "true");
		$response->headers->set('Access-Control-Max-Age', $this->maxAge());

		return $response;
	}
	protected function allowedOriginsToString(): string {
		if (!$this->isAllowed()) {
			return '';
		}

		if (in_array('*', $this->allowOrigins()) && ($this->request->header('Origin') === "")) {
			return '*';
		}

		return $this->request->header('Origin');
	}
}
