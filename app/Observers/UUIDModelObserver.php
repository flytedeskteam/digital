<?php

namespace App\Observers;

class UUIDModelObserver {

	public function creating($model) {
		$model->generateUuid();
	}

	public function saving($model) {
		$model->generateUuid();
	}

	public function updating($model) {
		$model->generateUuid();
	}
}
