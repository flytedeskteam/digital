<?php
namespace App\Repositories;
use App\Models\Impression;

/**
 * Created by PhpStorm.
 * User: tcobb
 * Date: 2018/09/11
 * Time: 04:17
 */

class ImpressionRepository {
	static function getClickTrackUrl(Impression $impression) {
		$url = url('click/' . $impression->uuid);

		if (env('APP_ENV') === 'production') {
			$url = preg_replace("/http:/", 'https:', $url);
		}

		return $url;
	}
}
