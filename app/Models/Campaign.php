<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Campaign
 *
 * @package Flytedesk
 */
class Campaign extends Model
{
	const
		TYPE_NATIONAL = 1,
		TYPE_LOCAL = 2;

	static $types = [
		self::TYPE_NATIONAL => 'National',
		self::TYPE_LOCAL    => 'Local',
	];

	const
		//When billing monthly, either bill this campaign ahead of time or after the ads run
		BILLING_POSTPAID = 0,
		BILLING_PREPAID = 1;

	const
		PAYMENT_CREDIT = 0,
		PAYMENT_INVOICE = 1;

	const
		STATUS_DRAFT = 0,
		STATUS_CREATIVE_PENDING = 1,
		STATUS_PROJECT_READY = 2,
		STATUS_CREATIVE_REJECTED = 3,
		STATUS_CANCELLED = 4,
		STATUS_NOT_READY = 5,
		STATUS_ON_HOLD = 6,
		STATUS_COMPLETE = 7,
		STATUS_ARCHIVED = 9,
		STATUS_DELETED = 10;

	//Campaign Makegood policy for when a publisher fails to run an ad, either re-run the ad or credit the buyer
	const
		MAKEGOOD_POLICY_RERUN = 0,
		MAKEGOOD_POLICY_CREDIT = 1;

	static $statuses = [
		self::STATUS_DRAFT             => 'Draft',
		self::STATUS_CREATIVE_PENDING  => 'Creative Pending',
		self::STATUS_PROJECT_READY     => 'Project Ready',
		self::STATUS_CREATIVE_REJECTED => 'Creative Rejected',
		self::STATUS_CANCELLED         => 'Cancelled',
		self::STATUS_NOT_READY         => 'Not Ready',
		self::STATUS_ON_HOLD           => 'On Hold',
		self::STATUS_COMPLETE          => 'Complete',
		self::STATUS_ARCHIVED          => 'Archived',
		self::STATUS_DELETED           => 'Deleted',
	];

	static $activeStatuses = [
		self::STATUS_CREATIVE_PENDING,
		self::STATUS_PROJECT_READY,
		self::STATUS_CREATIVE_REJECTED,
	];

	static $billableStatuses = [
		self::STATUS_CREATIVE_PENDING,
		self::STATUS_CREATIVE_REJECTED,
		self::STATUS_PROJECT_READY,
		self::STATUS_ON_HOLD,
		self::STATUS_NOT_READY,
		self::STATUS_COMPLETE,
	];

	static $ignoreStatuses = [
		self::STATUS_DRAFT,
		self::STATUS_DELETED,
	];

	static $publisherBookedStatuses = [
		self::STATUS_CREATIVE_PENDING,
		self::STATUS_CREATIVE_REJECTED,
		self::STATUS_PROJECT_READY,
		self::STATUS_CANCELLED,
		self::STATUS_ARCHIVED,
		self::STATUS_COMPLETE,
	];

	protected $table = 'campaigns';
}
