<?php

namespace App\Models;

use App\Models\Campaign\Ad;
use App\Traits\UUIDModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Impression extends Model
{
	use UUIDModel;
	protected $table = 'digital_impressions';

	public $timestamps = true;

	/* deny mass assignment to these */
	protected $guarded = ['id', 'date_created', 'date_updated'];

	const CREATED_AT = 'date_created';
	const UPDATED_AT = 'date_updated';

	const VISIBLE_RATIO = .5;

	protected $dates = [
		'date_created',
		'date_updated',
	];

	protected $casts = [
		'tags' => 'array',
	];

	public function ad() {
	    return $this->hasOne(Ad::class, 'id', 'ad_uuid');
    }

	public function clicks()
	{
		return $this->hasMany(Click::class, 'impression_uuid');
	}

	/**
	 * accessor for click_url that returns either the click_url or click_tag as appropriate
	 *
	 * @param $value
	 * @return string
	 */
	public function getClickUrlAttribute($value): string
	{
		// if no click_url then pull from our tags
		$url = $value;
		if (!$url)
		{
			$url = $this->click_tag;
		}

		return $this->sanitizeUrl($url);
	}

	/**
	 * accessor for image_url that returns either the image_url or creative_tag as appropriate
	 *
	 * @param $value
	 * @return string|null
	 */
	public function getImageUrlAttribute($value)
	{
		// if no click_url then pull from our tags
		$url = $value;
		if (!$url)
		{
			$url = $this->creative_tag;
		}

		return $url;
	}

	public function getClickTagAttribute()
	{
		$tag = null;
		if ($this->tags && array_key_exists('click', $this->tags))
		{
			$tag = $this->tags['click']['data'];
		}

		return $this->sanitizeUrl($tag);
	}

	public function getClickTagTypeAttribute()
	{
		// default is clickthru which is what click_url is
		$type = 'clickthru';
		if ($this->tags && array_key_exists('click', $this->tags) && $this->tags['click']['type'])
		{
			$type = $this->tags['click']['type'];
		}

		return $type;
	}

	public function getImpressionTagAttribute()
	{
		$tag = null;
		if ($this->tags && array_key_exists('impression', $this->tags))
		{
			$tag = $this->tags['impression']['data'];
		}

		return $tag;
	}

	public function getImpressionTagTypeAttribute()
	{
		$type = null;
		if ($this->tags && array_key_exists('impression', $this->tags))
		{
			$type = $this->tags['impression']['type'];
		}

		return $type;
	}

	public function getCreativeTagAttribute()
	{
		$tag = null;
		if ($this->tags && array_key_exists('creative', $this->tags))
		{
			$tag = $this->tags['creative']['data'];
		}

		return $tag;
	}

	/**
	 * accessor for creative_tag_type
	 *
	 * @return string
	 */
	public function getCreativeTagTypeAttribute()
	{
		// default is image as that's what image_url is...
		$type = 'image';
		if ($this->tags && array_key_exists('creative', $this->tags) && $this->tags['creative']['type'])
		{
			$type = $this->tags['creative']['type'];
		}

		return $type;
	}

	/**
	 * Track the analytical data for an impression (eg: clicks, visibility time, etc.)
     * Auto-verifies the Ad
	 *
	 * @param $unit
	 */
	public function track($unit)
	{
	    // Auto Verification of Ad
        $ad = $this->ad;
        if (isset($ad) && $ad->status === Ad::STATUS_UNVERIFIED) {
            $ad->status = Ad::STATUS_SUCCESS;
            $ad->save();
        }

		if (isset($unit['max_visible_ratio']) &&
			($unit['max_visible_ratio'] > $this->max_visible_ratio)
		)
		{
			$this->max_visible_ratio = $unit['max_visible_ratio'];

			// If the impression has not yet been considered viewed, lets check to see if it is now
			if (($this->was_viewed === 0) && ($this->max_visible_ratio >= self::VISIBLE_RATIO))
			{
				$this->markViewed();
			}
		}

		if (isset($unit['total_visible_time']) &&
			($unit['total_visible_time'] > $this->total_visible_time)
		)
		{
			$this->total_visible_time = $unit['total_visible_time'];
		}

		// Save changes if there were any
		$this->save();
	}

	/**
	 * Mark the Impression as viewed, but only if it was not previously viewed.
	 * In Addition, increment the # of impressions on the ad unit if it is associated to a paid ad.
	 */
	public function markViewed()
	{
		if (!$this->was_veiwed)
		{
			$this->was_viewed = 1;

			if ($this->asset_schedule_id)
			{
				// Count the impressions
				DB::table('asset_schedule')
					->where('id', '=', $this->asset_schedule_id)
					->increment('impression_count');
			}

			if ($this->ad_uuid)
			{
				// Count the impressions
				DB::table('ads')
					->where('id', '=', $this->ad_uuid)
					->increment('impression_count');
			}
		}
	}

	/**
	 * @param string $value
	 * @return string|null
	 */
	public function sanitizeUrl(string $value = null)
	{
		$url = '';
		if ($value !== null)
		{
			$parts = (object)parse_url($value);

			$url = @$parts->host . @$parts->path;

			// If there is no URL, we cannot link anywhere for this ad
			if (!$url)
			{
				return '';
			}

			$scheme = $parts->scheme ?? 'https';

			$url = "$scheme://$url";

			if (@$parts->query)
			{
				$url .= '?' . $parts->query;
			}

			if (@$parts->fragment)
			{
				$url .= '#' . $parts->fragment;
			}
		}

		return $url;
	}

}
