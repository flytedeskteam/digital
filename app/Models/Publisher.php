<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Publisher extends Model {
	use SoftDeletes;
	protected $table = 'publishers';
	public $timestamps = TRUE;
	
	public function mediums() {
		return $this->hasMany(Medium::class);
	}
}
