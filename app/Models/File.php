<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class File extends Model
{
    use SoftDeletes;

    protected $table = 'file';

    protected $fillable = [
        'disk',
        'filename',
        'filepath',
        'mime',
        'size',
        'storable_id',
        'storable_type'
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $appends = ['url'];

    public function storable()
    {
        return $this->morphTo();
    }

    /**
     * Generates the URL for a file from the storage system it was saved on.
     * Will default to using the ImgIx url if the disk it was stored on has not been specified.
     *
     * @return mixed|string
     */
    public function getUrlAttribute()
    {
        if ($this->disk) {
            return Storage::disk($this->disk)->url($this->filepath);
        } else {
            return Utilities::returnImgIxUrl($this->filepath);
        }
    }
}
