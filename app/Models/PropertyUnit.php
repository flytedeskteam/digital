<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyUnit extends Model {
	protected $table = 'property_units';

	public $timestamps = TRUE;

	/* deny mass assignment to these */
	protected $guarded = ['id', 'date_created', 'date_updated'];
	const CREATED_AT = 'date_created';
	const UPDATED_AT = 'date_updated';
	protected $dates = [
		'date_created',
		'date_updated'
	];
}
