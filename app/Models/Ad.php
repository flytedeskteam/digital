<?php

namespace App\Models\Campaign;

use App\Models\Creative;
use App\Models\Order\OrderProduct;
use Carbon\Carbon;
use Flytedesk\Buyer;
use Flytedesk\Publisher;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ad extends Model
{
    use SoftDeletes;

    protected $table = 'ads';

    static $refPrefix = 'A-';

    protected $fillable = [
        'start_date',
        'end_date',
        'success_requirements',
        'quantity',
        'impressions',
        'fulfillment_method',
        'status',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'start_date',
        'end_date',
        'deadline_date'
    ];

    protected $casts = [
        'fulfillment_method' => 'json',
    ];

    const
        STATUS_CANCELED = 'Canceled',
        STATUS_CREATIVE_PENDING = 'Creative Pending',
        STATUS_CREDITED = 'Credited',
        STATUS_MAKE_GOOD = 'Make Good',
        STATUS_PENDING_ACCEPTANCE = 'Pending Acceptance',
        STATUS_READY = 'Ready',
        STATUS_SUCCESS = 'Ad Ran',
        STATUS_UNVERIFIED = 'Unverified';

    const STATUSES = [
        self::STATUS_CANCELED,
        self::STATUS_CREATIVE_PENDING,
        self::STATUS_CREDITED,
        self::STATUS_MAKE_GOOD,
        self::STATUS_PENDING_ACCEPTANCE,
        self::STATUS_READY,
        self::STATUS_SUCCESS,
        self::STATUS_UNVERIFIED
    ];

    /**
     * The statuses that are Active for an ad before the ad run date
     */
    const ACTIVE_PRE_RUN_STATUSES = [
        self::STATUS_CREATIVE_PENDING,
        self::STATUS_PENDING_ACCEPTANCE,
        self::STATUS_READY
    ];

    /**
     * Statuses that are active for an ad after the run date
     */
    const ACTIVE_POST_RUN_STATUSES = [
        self::STATUS_SUCCESS,
        self::STATUS_UNVERIFIED
    ];

    /**
     * These Statuses no longer require action by the Publisher or the Buyer
     */
    const INACTIVE_STATUSES = [
        self::STATUS_CANCELED,
        self::STATUS_CREDITED,
        self::STATUS_MAKE_GOOD
    ];

    public function buyer()
    {
        return $this->belongsTo(Buyer::class);
    }

    public function publisher()
    {
        return $this->belongsTo(Publisher::class);
    }

    public function orderProduct()
    {
        return $this->belongsTo(OrderProduct::class);
    }

    public function creative()
    {
        return $this->belongsTo(Creative::class);
    }

    public function getFullNameAttribute()
    {
        return $this->orderProduct->full_name;
    }

    public function getMediumAttribute()
    {
        return $this->orderProduct->medium;
    }

    public function getPropertyAttribute()
    {
        return $this->orderProduct->property;
    }

    public function getCollectionAttribute()
    {
        return $this->orderProduct->collection;
    }

    public function getProductAttribute()
    {
        return $this->orderProduct->product;
    }

    public function getProductVariantAttribute()
    {
        return $this->orderProduct->product_variant;
    }

    public function isActivePreRunStatus()
    {
        return in_array($this->status, self::ACTIVE_PRE_RUN_STATUSES);
    }

    public function isActivePostRunStatus()
    {
        return in_array($this->status, self::ACTIVE_POST_RUN_STATUSES);
    }

    public function isActiveStatus()
    {
        return !$this->isInactiveStatus();
    }

    public function isInactiveStatus()
    {
        return in_array($this->status, self::INACTIVE_STATUSES);
    }

    /**
     * Checks if this ad runs for more than 1 day
     *
     * @return bool
     */
    public function isMultiDay()
    {
        return Carbon::parse($this->end_date)->diffInDays($this->start_date) >= 1;
    }
}
