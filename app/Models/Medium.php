<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Medium extends Model {
	protected $table = 'medium';
	public $incrementing = FALSE;
	public $timestamps = TRUE;
	
	const
			TYPE_PRINT = 'Print',
			TYPE_DIGITAL = 'Digital',
			TYPE_SOCIAL = 'Social',
			TYPE_STREET_TEAM = 'Street Team',
			TYPE_OOO = 'Out of Home';
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function publisher() {
		return $this->belongsTo(Publisher::class);
	}
	
	public function properties() {
		return $this->hasMany(Property::class);
	}
	
}
