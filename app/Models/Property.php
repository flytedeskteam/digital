<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Property extends Model {
	protected $table = 'property';
	public $incrementing = FALSE;
	public $timestamps = TRUE;
	
	
	public function medium() {
		return $this->belongsTo(Medium::class);
	}
	
}
