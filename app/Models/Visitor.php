<?php
namespace App\Models;

use App\Traits\UUIDModel;
use Illuminate\Database\Eloquent\Model;

class Visitor extends Model {
	use UUIDModel;
	protected $table = 'digital_visitors';

	public $timestamps = TRUE;

	/* deny mass assignment to these */
	protected $guarded = ['id', 'date_created', 'date_updated'];
	const CREATED_AT = 'date_created';
	const UPDATED_AT = 'date_updated';
	protected $dates = [
		'date_created',
		'date_updated'
	];
}
