<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UnitType extends Model {
	protected $table = 'digital_unit_types';

	public $timestamps = TRUE;

	/* deny mass assignment to these */
	protected $guarded = ['id', 'date_created', 'date_updated', 'deleted_at'];
	const CREATED_AT = 'date_created';
	const UPDATED_AT = 'date_updated';
	protected $dates = [
		'date_created',
		'date_updated'
	];
}
