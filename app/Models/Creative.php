<?php

namespace App\Models;

use App\Models\File;
use App\Models\Ad;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Creative extends Model
{
    use SoftDeletes;

    protected $table = 'creatives';

    protected $fillable = [
        'display_name',
        'creative_text',
        'clickthrough_link',
        'tracking_link',
        'status'
    ];

    const
        FILE_TYPE = 'creative_file';

    const
        STATUS_PENDING = 'Pending Creative',
        STATUS_PENDING_PUBLISHER = 'Pending Publisher',
        STATUS_PENDING_BUYER = 'Pending Buyer',
        STATUS_REJECTED = 'Rejected',
        STATUS_ACCEPTED = 'Accepted';

    public function files()
    {
        return $this->belongsToMany(File::class);
    }

    public function ads()
    {
        return $this->hasMany(Ad::class);
    }
    
}
