<?php
namespace App\Traits;

use Webpatser\Uuid\Uuid;

trait UUIDModel {

    /**
     * @param $value
     */
    public function setUuidAttribute($value) {
        // if null, or not a proper uuid sent, then generate one
        if (!isset($value) || !is_string($value) || (is_string($value) && strlen(trim($value)) !== 36)) {
            $this->generateUuid();
        } else {
            $this->attributes['uuid'] = $value;
        }
    }

    /**
     *
     */
    public function generateUuid() {
        if (!isset($this->attributes['uuid'])) {
            try {
                $this->attributes['uuid'] = (string)Uuid::generate(4);
            } catch (\Exception $e) {
            }
        }
    }
}
