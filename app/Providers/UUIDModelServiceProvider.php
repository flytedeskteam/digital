<?php

namespace App\Providers;

use App\Models\Creative;
use App\Models\Visitor;
use App\Models\Impression;
use App\Observers\UUIDModelObserver;
use Illuminate\Support\ServiceProvider;

class UUIDModelServiceProvider extends ServiceProvider {
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot() {
		Visitor::observe(UUIDModelObserver::class);
		Impression::observe(UUIDModelObserver::class);
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register() {
		//
	}
}
