<?php

namespace App\Http\Controllers;

use App\Models\Impression;
use App\Repositories\ImpressionRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AdController extends Controller
{
	const COOKIE_NAME = '_flyte';

	/**
	 * @param \Illuminate\Http\Request $request
	 * @param                          $impressionId
	 *
	 * @return Response
	 */
	public function adRequest(Request $request, $impressionId)
	{
		$this->logRequest($request);

		// find the impression
		/* @var Impression $impression */
		$impression = Impression::where('uuid', '=', $impressionId)
			->firstOrFail();

		return response(
			view('iframe',
				[
					'creative_type'   => $impression->creative_tag_type,
					'creative'        => $impression->image_url,
					'clickUrl'        => $impression->click_tag_type === 'clickthru' ? $impression->click_url : '#',
					'impression'      => $impression->impression_tag,
					'impression_type' => $impression->impression_tag_type,
					'width'           => $impression->width,
					'height'          => $impression->height,
					'clickTrackUrl'   => ImpressionRepository::getClickTrackUrl($impression),
				]
			)
		)
			->withHeaders(
				[
					'Expires'       => '0',
					'Cache-Control' => 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0',
					'Pragma'        => 'no-cache',
				]
			);
	}

	/**
	 * Track when an ad is clicked
	 *
	 * @param Request $request
	 * @param         $impressionId
	 */
	public function adClick(Request $request, $impressionId)
	{
		$impression = Impression::where('uuid', '=', $impressionId)->firstOrFail();

		$impression->clicks++;

		$impression->clicks()->create([
			'ip_address' => $request->getClientIp(),
		]);

		$impression->save();

		return response('OK')
			->withHeaders(
				[
					'Expires'       => '0',
					'Cache-Control' => 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0',
					'Pragma'        => 'no-cache',
				]
			);
	}

	/**
	 * Tracks the impression as viewed by the User and tallies the impression count for the ad
	 *
	 * @param Request $request
	 */
	public function adTracking(Request $request)
	{
		//Log::info("Ad Tracking: ", $request->all());
		$units = $request->input('units', []);
		if (\is_array($units))
		{
			foreach ($units as $unit)
			{
				if (isset($unit['id']))
				{
					DB::beginTransaction();
					$impression = Impression::where('uuid', '=', $unit['id'])->lockForUpdate()->first();
					if ($impression)
					{
						$impression->track($unit);
					}
					DB::commit();
				}
			}
		}

		return response('OK')
			->withHeaders(
				[
					'Expires'       => '0',
					'Cache-Control' => 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0',
					'Pragma'        => 'no-cache',
				]
			);
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 */
	protected function logRequest(Request $request)
	{
		Log::info("Ad Request: ", $request->all());
	}
}
