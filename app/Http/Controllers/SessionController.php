<?php

namespace App\Http\Controllers;

use App\Library\AdMediator;
use App\Models\Pageview;
use App\Models\Publisher;
use App\Models\Property;
use App\Models\Visitor;
use Carbon\Carbon;
use DateTimeZone;
use Illuminate\Cookie\CookieJar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SessionController extends Controller
{
	const COOKIE_NAME            = '_flyte';
	const COOKIE_EXPIRATION_DAYS = 365;

	/**
	 * @param \Illuminate\Http\Request     $request
	 * @param \Illuminate\Cookie\CookieJar $cookiejar
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function newAdSession(Request $request, CookieJar $cookiejar)
	{
		$this->logRequest($request);

		$pageviewId = $request->input('id');
		$event      = $request->input('event');

		// This can be either the Publisher UUID or the Property UUID
		$propertyUuid = $request->input("property");

		$session = $pageviewId ? Pageview::find($pageviewId) : null;

		// validate the property uuid, it is essential
		$publisher = Publisher::where('uuid', '=', $propertyUuid)->first();
		$property  = null;

		$cookie = null;

		if (!$publisher)
		{
			// this is the uuid for a property not a pub
			$property  = Property::with('medium.publisher')->where('id', '=', $propertyUuid)->firstOrFail();
			$publisher = $property->medium->publisher;
		}

		if (!$session)
		{
			$now     = Carbon::now(new DateTimeZone('America/Denver'));
			$nowDate = $now->toDateString();

			// identify the visitor
			$visitor = $this->identifyVisitor($request, $event);

			// create the visitor cookie
			$cookie = $cookiejar->make(self::COOKIE_NAME,
				$visitor->uuid,
				60 * 24 * self::COOKIE_EXPIRATION_DAYS,
				'/',
				'.flytedesk.com',
				false,
				false);

			// create a session and record the event details
			$session = $this->recordPageview($visitor->uuid, $publisher->uuid, $request->ip(), $event, $property ? $property->id : null);

			if ($property && $session->url !== null)
			{
				$parsed_url  = parse_url($session->url);
				$website_url = (isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : 'http://') .
					($parsed_url['host'] ?? '');

				$property->digital_seen_on = $nowDate;
				$property->website_url     = $website_url;
				$property->save();

				$publisher->digital_seen_on = $nowDate;
				$publisher->website_url     = $website_url;
				$publisher->save();
			}
		}

		// If any units were identified, they should be registered with the session
		$units          = $request->input('units', []);
		$unitsToDisplay = [];

		if (is_array($units) && (count($units) > 0))
		{
			$mediator       = new AdMediator();
			$unitsToDisplay = $mediator->getAds($session->id, $publisher, $units, $event, $property);
		}

		$json = [
			'id' => $session->id,
		];

		if (!empty($visitor))
		{
			$json['visitor'] = $visitor->uuid;
		}

		if ($unitsToDisplay)
		{
			$json['units'] = $unitsToDisplay;
		}

		$response = response()
			->json($json)
			->withHeaders(
				[
					'Expires'       => '0',
					'Cache-Control' => 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0',
					'Pragma'        => 'no-cache',
				]
			);

		if ($cookie)
		{
			$response->withCookie($cookie);
		}

		return $response;
	}

	/**
	 * Identify / create a new visitor record
	 *
	 * @param Request $request
	 * @param         $event
	 * @return Visitor
	 */
	protected function identifyVisitor(Request $request, $event)
	{
		$visitor_uuid = $request->filled('visitor')
			? $request->input('visitor', null)
			: $request->cookie(self::COOKIE_NAME, null);

		if ($visitor_uuid !== null)
		{
			$visitor = Visitor::where('uuid', '=', $visitor_uuid)
				->first();
		}
		if (!@$visitor)
		{
			$visitor = new Visitor;
		}

		if (isset($event['agent']))
		{
			$visitor->user_agent = $event['agent'];
		}
		if (isset($event['lang']))
		{
			$visitor->language = $event['lang'];
		}
		$visitor->save();

		return $visitor;
	}

	/**
	 * @param string      $visitor_uuid
	 * @param string      $publisher_uuid
	 * @param string      $ip
	 * @param array|NULL  $details
	 * @param string|NULL $property_uuid
	 *
	 * @return int
	 */
	protected function recordPageview(string $visitor_uuid, string $publisher_uuid, string $ip, array $details = null, string $property_uuid = null):
	Pageview
	{
		$session                 = new Pageview;
		$session->ip_address     = $ip;
		$session->visitor_uuid   = $visitor_uuid;
		$session->publisher_uuid = $publisher_uuid;
		$session->property_uuid  = $property_uuid;
		if ($details)
		{
			$session->meta     = $details["meta"];
			$session->referrer = $details["referrer"];

			$url = $details["url"];
			// remove anything from ? or # onward
			$pos = strpos($url, "?");
			if ($pos !== false)
			{
				$url = substr($url, 0, $pos);
			}
			$pos = strpos($url, "#");
			if ($pos !== false)
			{
				$url = substr($url, 0, $pos);
			}
			$session->url = $url;
		}
		$session->save();

		return $session;
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 */
	protected function logRequest(Request $request)
	{
		Log::info("Session Request: ", $request->all());
	}
}
