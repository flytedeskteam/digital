<?php

namespace App\Library;

use App\Models\Campaign;
use App\Models\Impression;
use App\Models\Publisher;
use App\Models\Property;
use DateTimeZone;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class AdMediator
{
	const UNITS_TO_PRODUCTS      = [
		'leaderboard'     => 25,
		'rectangle'       => 28,
		'large_rectangle' => 29,
		'halfpage'        => 32,
		'skyscraper'      => 30,
		'wide_skyscraper' => 31,
		'small_rectangle' => 27,
		'vertical_banner' => 26,
		'square'          => 33,
	];
	const UNITS_TO_PRODUCT_TYPES = [
		'leaderboard'     => '8b481edf-df94-49a7-864e-76dcc135c3f7',
		'rectangle'       => '8b481edf-dfa3-4ef5-9170-68e4b7a90cdd',
		'large_rectangle' => '8b481edf-dfa8-424d-b26e-d4c8014f0524',
		'halfpage'        => '8b481edf-dfb7-43e7-a7fd-0e14379759a2',
		'skyscraper'      => '8b481edf-dfad-4644-8846-a50abef2680a',
		'wide_skyscraper' => '8b481edf-dfb2-47d3-8273-c6e7dd39392f',
		'small_rectangle' => '8b481edf-df9e-4338-a62f-1ddc03cdf917',
		'vertical_banner' => '8b481edf-df99-4051-8a2b-1e4a21d3e9fb',
		'square'          => '8b481edf-dfbc-45a6-93c0-198c73e5c68c',
	];
	const TEXT_TO_UNITS          = [
		'leaderboard'     => 1,
		'rectangle'       => 2,
		'large_rectangle' => 3,
		'halfpage'        => 4,
		'skyscraper'      => 5,
		'wide_skyscraper' => 6,
		'small_rectangle' => 7,
		'vertical_banner' => 15,
		'square'          => 16,
	];
	const DIGITAL_MEDIAKIT_ID    = 3;
	const DIGITAL_MEDIUM_TYPE    = '8b480858-cb0b-44aa-8000-b6ca7c23c026';

	protected $stage_prefix = "";

	/**
	 * AdMediator constructor.
	 */
	public function __construct()
	{
		$this->stage_prefix = env("S3_BUCKET_URL");

	}

	/**
	 * @param int                       $pageview_id
	 * @param \App\Models\Publisher     $publisher
	 * @param array                     $units
	 * @param array                     $event
	 * @param \App\Models\Property|NULL $property
	 * @return array
	 */
	public function getAds(
		int $pageview_id,
		Publisher $publisher,
		array $units,
		array $event,
		Property $property = null
	): array
	{
		/*
		 * the array format we got is
		 *		id: flyteBlocks[i].id,
					type: unitType,
					w: unitWidth,
					h: unitHeight,
					visible: window.f

					we send it back with the same fields plus
						impression:  uuid
					and if it is a remnant, then
						remnant: true
		 */

		$results = [];
		//now, but mountain time
		$now         = Carbon::now(new DateTimeZone('America/Denver'));
		$nowDate     = $now->toDateString();
		$nowDateTime = $now->toDateTimeString();


		foreach ($units as $unit)
		{
			$advert            = null;
			$asset_schedule_id = null;
			$ad_uuid           = null;
			$campaign_id       = null;
			$campaign_uuid     = null;
			$impression        = null;

			if ($property !== null)
			{
				// v3 mediakit -- look in the ads table
				$advert = DB::table('ads')
					->select(
						'ads.id as ad_uuid',
						'ads.buyer_id',
						'ads.campaign_id as campaign_uuid',
						'ads.creative_id',
						'ads.impressions',
						'ads.start_date',
						'ads.end_date',
						'ads.impression_count',
						'creatives.clickthrough_link as click_url',
						'creatives.digital_tags',
						'file.filepath as filename'
					)
					->leftJoin('creatives',
						function ($join) {
							$join->on('ads.creative_id', '=', 'creatives.id')
								->join('creative_file', 'creatives.id', '=', 'creative_file.creative_id')
								->join('file', 'creative_file.file_id', '=', 'file.id')
								->whereNull('file.deleted_at');
						})
					->join('order_product',
						function ($join) use ($property, $unit) {
							$join->on('ads.order_product_id', '=', 'order_product.id')
								->where('order_product.medium_type_id', '=', self::DIGITAL_MEDIUM_TYPE)
								->where('order_product.property_id', '=', $property->id)
								->where('order_product.product_type_id', '=', self::UNITS_TO_PRODUCT_TYPES[$unit['type']]);
						})
					->where('ads.start_date', '<=', $nowDateTime)
					->where('ads.end_date', '>', $nowDateTime)
					->whereNotNull('ads.creative_id')
					->whereNull('ads.deleted_at')
					->where(function ($query) {
						$query->where('ads.impression_limit_reached', '=', false)
							->orWhere('ads.impressions', '=', 0);
					})
					->inRandomOrder()
					->first();
				if ($advert)
				{
					$ad_uuid       = $advert->ad_uuid;
					$campaign_uuid = $advert->campaign_uuid;
				}
			}
			// check for the publisher id on the asset_schedule table even if we have a property
			if (!$advert)
			{
				$advert = DB::table('asset_schedule as as')
					->select(
						'as.id as asset_schedule_id',
						'as.buyer_id',
						'as.campaign_id',
						'as.asset_date',
						'as.asset_status',
						'as.asset_id',
						'as.photoset_id',
						'as.click_url',
						'as.digital_tags',
						'as.impression_qty',
						'as.impression_count',
						'as.quantity',
						'assets.filename'
					)
					->leftJoin('assets', 'as.asset_id', '=', 'assets.id')
					->join('campaigns',
						function ($join) use ($nowDate) {
							$join->on('as.campaign_id', '=', 'campaigns.id')
								->whereIn('campaigns.campaign_status', Campaign::$activeStatuses)
								->where('campaigns.campaign_end', '>=', $nowDate);
						})
					->where('as.publisher_id', '=', $publisher->id)
					->where('as.product_id', '=', self::UNITS_TO_PRODUCTS[$unit['type']])
					->where('as.mediakit_id', '=', self::DIGITAL_MEDIAKIT_ID)
					->where('as.asset_date', '<=', $nowDate)
					->where('as.creative_status_id', '=', 3)
					->where(function ($query) use ($nowDate) {
						$query->where('as.impression_limit_reached', '=', false)
							->orWhereRaw('DATE_ADD(as.asset_date, INTERVAL as.quantity DAY) > ?', [$nowDate]);
					})
					->inRandomOrder()
					->first();
				if ($advert)
				{
					$asset_schedule_id = $advert->asset_schedule_id;
					$campaign_id       = $advert->campaign_id;
				}
			}

			// Grab the existing impression if it exists, so we do not duplicate an ad impression
			$impression = Impression::where('ad_dom_id', $unit['id'])
				->where('pageview_id', $pageview_id)
				->first();

			if (!$impression)
			{
				$impression               = new Impression();
				$impression->pageview_id  = $pageview_id;
				$impression->ad_dom_id    = $unit['id'];
				$impression->unit_type_id = self::TEXT_TO_UNITS[$unit['type']];
				$impression->publisher_id = $publisher->id;
				$impression->width        = $unit['w'];
				$impression->height       = $unit['h'];
				$impression->is_real      = $this->isRealImpression($event);

				$digital_tag_array = null;
				if ($advert && $advert->digital_tags)
				{
					$digital_tag_array = json_decode($advert->digital_tags, true);
				}

				if (
					$advert && ($advert->filename ||
						(\is_array($digital_tag_array) && $this->tagIncludesCreative($digital_tag_array)))
				)
				{
					if ($advert->filename)
					{
						$impression->image_url = $this->stage_prefix . $advert->filename;
					}
					$impression->click_url         = $advert->click_url;
					$impression->buyer_id          = $advert->buyer_id;
					$impression->campaign_id       = $campaign_id;
					$impression->campaign_uuid     = $campaign_uuid;
					$impression->asset_schedule_id = $asset_schedule_id;
					$impression->ad_uuid           = $ad_uuid;
					$impression->tags              = $digital_tag_array;
				}
				else
				{
					$impression->remnant   = 1;
					$impression->image_url = '';
					$impression->click_url = '';
					$unit['remnant']       = true;
				}

				$impression->save();
			}

			$unit['impression'] = $impression->uuid;

			$results[] = $unit;
		}

		return $results;
	}

	/**
	 * For now, this simply checks if the impression is coming from flytedesk.com domain (for production)
	 * and if it is, flags the impression as a non-real impression so it won't be counted in analytics
	 *
	 * @param $event
	 * @return bool
	 */
	public function isRealImpression($event): bool
	{
		if (env('APP_ENV') === 'production')
		{
			$host = parse_url(@$event['url'], PHP_URL_HOST);

			if (preg_match("/flytedesk.com$/", $host))
			{
				return false;
			}
		}

		return true;
	}

	/**
	 * @param array $digital_tag_array
	 * @return bool
	 */
	private function tagIncludesCreative(array $digital_tag_array): bool
	{
		return ($digital_tag_array &&
			array_key_exists('creative', $digital_tag_array) &&
			array_key_exists('data', $digital_tag_array['creative']) &&
			isset($digital_tag_array['creative']['data']));
	}

}
