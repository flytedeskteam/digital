let mix = require('laravel-mix');

mix.webpackConfig({
	mode: 'development'
});

mix.babel(['resources/assets/js/head.js'], 'public/js/head.js').sourceMaps();
mix.babel(['resources/assets/js/head-staging.js'], 'public/js/head-staging.js').sourceMaps();
mix.babel(['resources/assets/js/head-dev.js'], 'public/js/head-dev.js').sourceMaps();
