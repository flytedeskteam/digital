<!DOCTYPE html>
<html lang="en">
<head>
	<title>Flytedesk</title>
	<base target="_top">
	<script>
    function trackClick () {
      let request = new XMLHttpRequest();
      request.open('GET', "<?php echo $clickTrackUrl ?>");
      request.send();
    }

    function trackImpression () {
      let request = new XMLHttpRequest();
      request.open('GET', "<?php echo @$impression ?>");
      request.send();
    }
	</script>
</head>
<body style="margin:0;padding:0;background:transparent">

<?php if ($clickUrl): ?>
<a target="_blank" href="<?php echo $clickUrl; ?>" onclick="trackClick()">
	<?php endif ?>

	<?php if ($creative_type === 'image'): ?>
		 <img height="<?php echo $height; ?>" width="<?php echo $width; ?>" border="0" src="<?php echo $creative; ?>"/>
	<?php else: ?>
		<?php echo $creative; ?>
	<?php endif ?>

	<?php if ($clickUrl): ?>
</a>
<?php endif ?>

<?php if ($impression): ?>
	<?php if ($impression_type === 'image'): ?>
		<img src="<?php echo $impression; ?>"/>
	<?php else: ?>
		<script>trackImpression();</script>
	<?php endif ?>
<?php endif ?>

</body>
</html>

