if (!window.DIGITAL_REMOTE_URL) {
	throw new Error("You must provide the DIGITAL_REMOTE_URL const to serve ads in the Dev environment");
}

window.flyte = window.flyte || {};
(function () {
	"use strict";
	let $fd;

	$fd = {
		propertyId:        '',
		visitorId:         '',
		scriptId:          'flytedigital',
		ourClass:          'flytead',
		remote:            window.DIGITAL_REMOTE_URL,
		iframeUri:         '/ad/',
		sessionUri:        '/session',
		trackingUri:       '/track',
		cookieName:        '_flyte',
		remnantSrc:        'https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js',
		imagePixels:       [
			'https://www.facebook.com/tr?id=605179343215247&ev=PageView'
		],
		eventDetails:      {},
		// Retries to locate the ad units if they are not present
		attempts:          50,
		// Update the tracking data every 2 seconds (to record visibility time)
		updateInterval:    5000,
		// The ratio of an ad's visibility to count as an impression
		intersectionRatio: .5,
		units:             [],
		unitElements:      [],
		cookieRegex:       /(?:(?:^|.*;\s*)_flyte\s*=\s*([^;]*).*$)|^.*$/,
		unitTypes:         {
			leaderboard:     {
				w:  728,
				h:  90,
				ad: "5389905163"
			},
			rectangle:       {
				w:  300,
				h:  250,
				ad: "6343894181"
			},
			large_rectangle: {
				w:  336,
				h:  280,
				ad: "4131813756"
			},
			halfpage:        {
				w:  300,
				h:  600,
				ad: "3720750217"
			},
			skyscraper:      {
				w:  120,
				h:  600,
				ad: "3800232148"
			},
			wide_skyscraper: {
				w:  160,
				h:  600,
				ad: "7082260788"
			},
			small_rectangle: {
				w:  180,
				h:  150,
				ad: "4019723996"
			},
			vertical_banner: {
				w:  120,
				h:  240,
				ad: "9763005267"
			},
			square:          {
				w:  250,
				h:  250,
				ad: "4974972199"
			}
		},

		documentReadyListener: function () {
			// Identify unit is now asynchronous (thus the subsequent methods are called at the end)
			$fd.identifyUnits();
		},

		identifyUnits: function () {
			let flyteBlocks = document.getElementsByClassName($fd.ourClass), i, j, unitType,
				unitHeight, unitWidth, classArray;

			// Keep trying if the blocks are loaded after the page loads
			if (!flyteBlocks.length && $fd.attempts-- > 0) {
				return setTimeout($fd.identifyUnits, 100);
			}

			for (i = 0; i < flyteBlocks.length; i++) {
				let adUnit = flyteBlocks[i];

				unitType = null;
				// we need to get the id and the additional class of this element
				classArray = adUnit.className.split(' ');

				for (j = 0; j < classArray.length; j++) {
					// find out which kind of unit this is... looking for unitClassPrefix
					if ($fd.unitTypes.hasOwnProperty(classArray[j])) {
						unitType = classArray[j];
						unitWidth = $fd.unitTypes[unitType].w;
						unitHeight = $fd.unitTypes[unitType].h;
						break;
					}
				}

				if (unitType) {
					// this is a real ad unit
					if (adUnit.id.trim().length === 0) {
						// doesn't have its own ID
						adUnit.id = $fd.uuidv4();
					}
					$fd.units.push(
						{
							id:      adUnit.id,
							type:    unitType,
							w:       unitWidth,
							h:       unitHeight,
							visible: $fd.isInViewport(adUnit)
						}
					);

					$fd.unitElements.push(adUnit);
				}
			}

			// Do the rest of the things
			$fd.getEventDetails();
			$fd.firePixels();
			$fd.getSessionImpressions();
			$fd.trackVisibility();
		},

		pushAds: function () {
			// ready to go, so find our elements
			let i, hasRemnants = false;
			for (i = 0; i < $fd.units.length; i++) {
				// we're only going to do something if we have an impression id
				if ($fd.units[i].hasOwnProperty('impression') &&
					$fd.units[i].hasOwnProperty('id') &&
					$fd.units[i].hasOwnProperty('type') &&
					$fd.units[i].hasOwnProperty('w') &&
					$fd.units[i].hasOwnProperty('h')) {
					let el = document.getElementById($fd.units[i].id);
					if (el) {
						if ($fd.units[i].hasOwnProperty('remnant')) {
							hasRemnants = true;
							// insert code inside the el to identify the ad unit
							$fd.insertRemnantId(el, $fd.units[i].type);
						} else {
							// now we have everything we need
							if (el) {
								let iFrame = document.createElement('iframe');
								iFrame.scrolling = "no";
								iFrame.frameBorder = "0";
								iFrame.marginWidth = "0";
								iFrame.marginHeight = "0";
								iFrame.width = $fd.units[i].w.toString();
								iFrame.height = $fd.units[i].h.toString();
								iFrame.src = $fd.remote +
									$fd.iframeUri +
									$fd.units[i].impression;
								el.appendChild(iFrame);
							}
						}
					}
				}
			}
			if (hasRemnants) {
				// load third party ad server code
				$fd.loadRemnantService();
			}
		},

		firePixels: function () {
			let i;
			for (i = 0; i < $fd.imagePixels.length; i++) {
				let imageElement = new Image();
				imageElement.width = 1;
				imageElement.height = 1;
				imageElement.src = $fd.imagePixels[i];
			}
		},

		insertRemnantId: function (parent, unitType) {
			if ($fd.unitTypes.hasOwnProperty(unitType)) {
				let el = window.document.createElement('ins');
				el.className = "adsbygoogle";
				el.setAttribute('data-ad-client', 'ca-pub-6349992477875006');
				el.setAttribute('data-ad-slot', $fd.unitTypes[unitType].ad);
				el.setAttribute('style', 'display:inline-block;width:' +
					$fd.unitTypes[unitType].w +
					'px;height:' +
					$fd.unitTypes[unitType].h + 'px');
				parent.appendChild(el);
				let script = document.createElement("script");
				script.text = "(adsbygoogle = window.adsbygoogle || []).push({});";
				parent.appendChild(script);
			}
		},

		loadRemnantService: function () {
			let remnantScript = document.createElement("script");
			remnantScript.async = true;
			remnantScript.src = $fd.remnantSrc;
			document.body.appendChild(remnantScript);
		},

		continueInit:    function () {
			if (document.readyState === "complete" || (document.readyState !== "loading" && !document.documentElement.doScroll)) {
				$fd.documentReadyListener();
			} else {
				document.addEventListener("DOMContentLoaded", $fd.documentReadyListener());
			}
		},
		init:            function () {
			if ($fd.getPropertyId()) {
				$fd.getVisitorId();
				$fd.continueInit();
			} else {
				console.log(
					'ERROR: Property ID is required for the initialization of this script.  Property ID was not found.  Please contact Flytedesk support.');
			}

		},
		// collect the event details like referrer, url, etc.
		getEventDetails: function () {
			let metaCollection = document.getElementsByTagName('meta'), i, metas = {}, name;

			for (i = 0; i < metaCollection.length; i++) {
				name = (metaCollection[i].name || metaCollection[i].getAttribute("property"));
				if (name) {
					metas[name] = metaCollection[i].getAttribute("content");
				}
			}

			$fd.eventDetails = {
				url:      window.location.href,
				referrer: window.document.referrer,
				meta:     metas,
				agent:    window.navigator.userAgent,
				lang:     window.navigator.language
			};
		},

		/**
		 * Post data to the flytedesk digital backend.
		 *
		 * @param uri
		 * @param data
		 * @param onSuccess
		 */
		post: function (uri, data, onSuccess) {
			let request = new XMLHttpRequest();

			if (onSuccess) {
				request.onreadystatechange = function () {
					if (request.status === 200 && request.readyState === 4) {
						onSuccess(request);
					}
				};
			}

			// open the request to the specified source
			request.open('POST', $fd.remote + uri, true);
			request.setRequestHeader('Content-Type', 'application/json');
			// allow cookies to be set
			request.withCredentials = true;

			// execute the request
			request.send(JSON.stringify(data));
		},

		/**
		 * Get the ads for the ad units that were found on the page, and setup the session / impressions for each
		 */
		getSessionImpressions: function () {
			let data = {
				visitor:  $fd.visitorId,
				property: $fd.propertyId,
				units:    $fd.units,
				event:    $fd.eventDetails
			};

			$fd.post($fd.sessionUri, data, function (request) {
				$fd.receiveSessionImpressions(request.responseText);
			});
		},

		receiveSessionImpressions: function (data) {
			if (data) {
				try {
					let sessionData = JSON.parse(data);
					if (sessionData.hasOwnProperty('visitor')) {
						$fd.visitorId = sessionData.visitor;
						$fd.setVisitorCookie();
					}
					if (sessionData.hasOwnProperty('units')) {
						$fd.units = sessionData.units;

						// Update the elements so they know which impression ID they are tracking
						for (let i in $fd.units) {
							let unit = $fd.units[i];
							for (let j in $fd.unitElements) {
								let el = $fd.unitElements[j];
								if (el.id === unit.id) {
									el.dataset.impression = unit.impression;
								}
							}
						}

						// Be sure to send the tracking data immediately before the user leaves the page
						$fd.updateTracking();
					}
				}
				catch (err) {
					console.log('Bad data received: ' + data);
				}

			} else {
				console.log('no session returned from ajax');
			}
			$fd.pushAds();
		},

		updateTracking: function () {
			let data = {
				units: []
			};

			let now = performance.now();

			for (let unitId in $fd.unitElements) {
				let adUnit = $fd.unitElements[unitId];

				// If this unit is not yet tracked to an impression, we cannot update it
				if (adUnit.dataset.impression) {
					let total_visible_time = +adUnit.dataset.totalVisibleTime;
					let max_visible_ratio = +adUnit.dataset.maxVisibleRatio;
					// let clicks = +adUnit.dataset.clicks;

					// Add up the current total visible time for this ad
					if (adUnit.dataset.lastViewStarted) {
						total_visible_time += now - +adUnit.dataset.lastViewStarted;

						adUnit.dataset.totalVisibleTime = total_visible_time;
						adUnit.dataset.lastViewStarted = now;
					}

					data.units.push({
						id: adUnit.dataset.impression,
						total_visible_time,
						max_visible_ratio
						// clicks
						// TODO could probably ad more tracking data here...
					});
				}
			}

			$fd.post($fd.trackingUri, data);
		},

		trackVisibility: function () {
			let adObserver;

			let observerOptions = {
				root:       null,
				rootMargin: "0px",
				// Track every 20% change (including the 50% mark) of visibility per ad
				threshold:  [0.0, 0.2, 0.4, 0.5, 0.6, 0.8, 1.0]
			};

			adObserver = new IntersectionObserver(intersectionCallback, observerOptions);

			for (let unitId in $fd.unitElements) {
				let adUnit = $fd.unitElements[unitId];
				// initialize our visibility timer
				adUnit.dataset.totalVisibleTime = 0;
				adUnit.dataset.maxVisibleRatio = 0;
				// adUnit.dataset.clicks = 0;

				// Observe visibility changes for the ad unit
				adObserver.observe(adUnit);

				// TODO need a very robust iframe click tracking handler..
				// adUnit.addEventListener('click', function (e) {
				// 	adUnit.dataset.clicks = +adUnit.dataset.clicks + 1;
				// 	$fd.updateTracking();
				// }, true);
			}

			/**
			 * The callback whenever an ad becomes either visible / hidden on the screen
			 *
			 * TODO: We can do a lot more here, tracking % of 100% visibility vs partial visibility
			 * @param entries
			 */
			function intersectionCallback(entries) {
				entries.forEach(function (entry) {
					let adUnit = entry.target;

					// Record the largest visible portion of the ad at any one time
					adUnit.dataset.maxVisibleRatio = Math.max(adUnit.dataset.maxVisibleRatio, entry.intersectionRatio);

					// If the ad is considered visible and counts as an impression, lets track that
					if (entry.isIntersecting && entry.intersectionRatio >= $fd.intersectionRatio) {
						// Update the last viewed time as we have already banked the time from the previous view
						adUnit.dataset.lastViewStarted = entry.time;
					} else {
						// If this ad was previously visible, lets bank the total visible time
						if (adUnit.dataset.lastViewStarted) {
							adUnit.dataset.totalVisibleTime = +adUnit.dataset.totalVisibleTime + entry.time - +adUnit.dataset.lastViewStarted;
						}

						// We are no longer adding to the total visible time (empty string is false)
						adUnit.dataset.lastViewStarted = '';
					}
				});

				$fd.updateTracking();
			}

			// Regularly send out tracking data to our server to track total visible timing
			window.setInterval($fd.updateTracking, $fd.updateInterval);

			// When the document itself is no longer visible, we want to bank the visible time and pause timing
			document.addEventListener("visibilitychange", handleVisibilityChange, false);

			function handleVisibilityChange() {
				// Only update the tracking when the document goes from visible to hidden,
				// no new information will be available for the opposite
				if (document.hidden) {
					$fd.updateTracking();
				}

				for (let i in $fd.unitElements) {
					let el = $fd.unitElements[i];

					if (document.hidden) {
						// If the element was visible before the document was hidden
						el.dataset.wasVisible = el.dataset.lastViewStarted ? 1 : '';
						el.dataset.lastViewStarted = '';
					} else {
						// Start tracking the visible timing again
						if (+el.dataset.wasVisible) {
							el.dataset.lastViewStarted = performance.now();
						}
					}
				}
			}
		},

		setVisitorCookie: function () {
			// set local cookie named "sc" with a 1 year expiration
			// TODO:  set domain (to TLD)
			let yearDate = new Date(Date.now() + 31556952000),
				cookieValue = $fd.cookieName +
					"=" +
					encodeURIComponent($fd.visitorId) +
					";path=/;max-age=31536000;expires=" +
					yearDate.toUTCString();
			document.cookie = cookieValue;
		},

		// find out if the element is above or below the 'fold'
		// call it true if at least half of the div is visible
		isInViewport: function (elem) {
			let bounding = elem.getBoundingClientRect(),
				viewportHeight = (window.innerHeight || document.documentElement.clientHeight),
				viewportWidth = (window.innerWidth || document.documentElement.clientWidth),
				elemHeight = Math.abs(bounding.bottom - bounding.top),
				elemWidth = Math.abs(bounding.right - bounding.left);
			return (
				(bounding.top + window.pageYOffset) >= 0 &&
				(bounding.left + window.pageXOffset) >= 0 &&
				((bounding.bottom + window.pageYOffset) <= viewportHeight ||
					(bounding.bottom + window.pageYOffset - elemHeight / 2) <= viewportHeight) &&
				((bounding.right + window.pageXOffset) <= viewportWidth ||
					(bounding.right + window.pageXOffset - elemWidth / 2) <= viewportWidth)
			);
		},

		getPropertyId: function () {
			let propId = null,
				src = document.getElementById($fd.scriptId).src,
				results = src.split('#', 2);

			if (results && results[1]) {
				propId = decodeURIComponent(results[1]);
			}
			if (propId !== null) {
				$fd.propertyId = propId;
			}
			return (propId !== null);
		},
		// retrieves the local visitor uuid from cookies
		getVisitorId:  function () {
			let visitor_uuid = document.cookie.replace($fd.cookieRegex, "$1") || "";
			visitor_uuid = visitor_uuid.trim();
			if (visitor_uuid && (visitor_uuid.length > 0)) {
				$fd.visitorId = visitor_uuid;
			}
		},
		uuidv4:        function () {
			return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
				let r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
				return v.toString(16);
			});
		}
	};

	window.flyte.digital = $fd;
	$fd.init();
})();
