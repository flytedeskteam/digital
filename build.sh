#!/bin/bash

timestamp() {
	date +"%Y-%m-%d-%H-%M"
}

yarn run production

zip -r "digital-app-$(timestamp).zip" app/ bootstrap/ config/ database/ docs/ environment/ public/ resources/ routes/ storage/ support/ .ebextensions/ composer.json composer.lock
