<?php

$router->get('/health', 'HealthController');

$router->get('/ad/{impressionId}', 'AdController@adRequest');

$router->post('/track', 'AdController@adTracking');

$router->get('/click/{impressionId}', 'AdController@adClick');

$router->get('/link/{impressionId}', 'AdController@linkProxy');

$router->post('/session', 'SessionController@newAdSession');
